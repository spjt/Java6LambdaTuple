

import java.io.Serializable;
import java.util.List;

/**
 * Created by phil on 12/18/14.
 */
public interface Tuple extends Serializable {
	public List<?> asList();
	public Object[] asArray();
}
