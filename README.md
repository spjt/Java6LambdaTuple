 Various classes to simulate features of functional languages

 Your company won't upgrade to Java 8? Use these classes to gain some of the benefits. Everything will still work if/when you upgrade. If you
 use IntelliJ, it will even collapse these into Java 8-style lambda expressions. If you do use Java 8 but don't use Lambdas, maybe this will
 help you understand them better by simulating them in a familiar style. 

 LambdaX<T,...>: Simulate a lambda expression of X arguments.
 These are abstract classes that can be implemented anonymously with a standard apply method. The first generic type is the return type,
 the remaining generic types are the types of the arguments in order.
 e.g.

      public class LambdaExample {
        Lambda1<Integer, Integer> square = new Lambda1<Integer, Integer>(){
            public Integer apply(Integer integer) {
                return integer * integer;
            }
        };
        Lambda1<Integer, Integer> cube = new Lambda1<Integer, Integer>() {
            public Integer apply(Integer integer) {
                return integer * integer * integer;
            }
        };
        public Integer useLambda(Integer i, Lambda1<Integer, Integer> l) {
            return l.apply(i);
        }
        public void testLambda() {
            int testInt = 6;
            System.out.println("Squared: " + useLambda(testInt, square));
            System.out.println("Cubed: " + useLambda(testInt, cube));
        }
      }
    
 Lambda2 can be curried:

    Lambda2<String, Integer, String> repeatString = new Lambda2<String, Integer, String>() {
        @Override
        public String apply(Integer integer, String string) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < integer; i++) {
                sb.append(string);
            }
            return sb.toString();
        }
    };

    Lambda1<String, String> repeat5Times = repeatString.apply(5);
    repeat5Times.apply("hello");

 TupleX<T,...>: Simulate a tuple.
 Similar but opposite to a list. A list has a fixed type and variable size, but a tuple has fixed size and variable type.
 e.g.

        Tuple2<Integer, String> tuple = new Tuple2<>(1, "Hello");

 Lambdas and Tuples can be combined to form Lambdas that take a variable number of arguments via the Tuple interface and
 asList/asArray methods.

 The initial implementation contains only 2-tuples and 0-2 argument lambdas. Implementing higher order lambdas/tuples should
 be self-explanatory from these. Please see the highest-ordered tuple/lambda to see how
 (for example only the Lambda2 supports currying)
