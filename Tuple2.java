

import java.util.Arrays;
import java.util.List;

/**
 * Created by phil on 12/18/14.
 */
public class Tuple2<T,U> implements Tuple {
	private T _1;
	private U _2;

	public Tuple2(T _1, U _2) {
		this._1 = _1;
		this._2 = _2;
	}

	public T get1() {
		return _1;
	}

	public U get2() {
		return _2;
	}

	public T _1() {
		return _1;
	}

	public U _2() {
		return _2;
	}

	public void set1(T _1) {
		this._1 = _1;
	}

	public void set2(U _2) {
		this._2 = _2;
	}

	public List<?> asList() {
		return Arrays.asList(asArray());
	}

	public Object[] asArray() {
		return new Object[]{_1, _2};
	}
}
