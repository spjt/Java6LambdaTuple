

/**
 * Created by phil on 12/18/14.
 */
public abstract class Lambda2<T,U,V> {
	public abstract T apply(U u, V v);

	// Currying
	public Lambda1<T,V> apply(final U u) {
		final Lambda2<T,U,V> outer = this;
		return new Lambda1<T, V>() {
			@Override
			public T apply(V v) {
				return outer.apply(u, v);
			}
		};
	}

}
